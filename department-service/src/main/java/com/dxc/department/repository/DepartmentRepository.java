package com.dxc.department.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dxc.department.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department,Long> {

	Department findByDepartmentId(Long departmentId);

}
